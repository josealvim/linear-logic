\documentclass[main.tex]{subfile}

\begin{document}
	\begin{definition}[\(\lequiv\)]
		\[
			\alpha\lequiv\beta 
			\stackrel{\text{def.}}\iff 
			(\judgement\alpha\beta) 
			\ 
			\text{and} 
			\
			(\judgement\beta\alpha)
			\
			\text{are theorems}
		\]
	\end{definition}
	
	\begin{theorem}
		\label{thm:lequiv congruence}
		The relation \(\lequiv\) is a congruence: it is obviously 
		symmetric and reflexive; transitivity is obtained as follows:
		if 
		\[
			\alpha\lequiv\beta\lequiv\gamma
		\]
		then 
		\[
			\binaryinf[\nameref{ax:cut}]{
				\judgement\alpha\beta
			}{
				\judgement\beta\gamma
			}{
				\judgement\alpha\gamma
			}
		\]
		Moreover,
		\begin{equation}
			\label{thm:lequiv congruence:congruence}
			\binaryinf[\nameref{ax:cut}]{	
				\judgement\alpha\beta
			}{
				\judgement
					{\Gamma,\beta,\Delta}
					{\Phi}
			}{
				\judgement
					{\Gamma,\alpha,\Delta}
					{\Phi}
			}
			\qquad
			\binaryinf[\nameref{ax:cut}]{	
				\judgement
					{\Gamma}
					{\alpha,\Phi}
			}{
				\judgement\alpha\beta
			}{
				\judgement
					{\Gamma}
					{\beta,\Phi}
			}
		\end{equation}
		Do note that if 
		\[
			\phi\sim\psi\stackrel{\text{def.}}\iff
			\forall\Gamma,\Delta:(
				[\judgement\Gamma{\phi,\Delta}]
				\iff
				[\judgement\Gamma{\psi,\Delta}]
			)
		\]
		then 
		\[
			\inference[\(\phi\sim\psi\)]{
				\inference[\nameref{ax:identity}]{}{
					\judgement\phi\phi
				}
			}{
				\judgement\psi\phi
			}
			\qquad
			\inference[\(\phi\sim\psi\)]{
				\inference[\nameref{ax:identity}]{}{
					\judgement\psi\psi
				}
			}{
				\judgement\phi\psi
			}
		\]
		and thus
		\[
			\phi\sim\psi\iff\phi\lequiv\psi
		\]
	\end{theorem}


	\begin{theorem}
		\label{thm:dual universal}
		\(\dual\_\) is universal wrt. its rules. Suppose the same rules
		hold for a different connective “\(\_^*\)”
		\[
			\inference[\(^*\)L]{
				\inference[\nameref{ax:right dual}]{
					\inference[\nameref{ax:identity}]{}{
						\judgement\phi\phi
					}
				}{
					\judgement{}{\dual\phi,\phi}
				}
			}{
				\judgement{\phi^*}{\dual\phi}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:tens universal}
		\(\tens\) is universal wrt. its rules.
		\[
			\inference[\(\boxtimes\)L]{
				\binaryinf[\nameref{ax:right tens}]{
					\judgement\phi\phi
				}{
					\judgement\psi\psi
				}{
					\judgement{\phi,\psi}{\phi\tens\psi}
				}
			}{
				\judgement{\phi\boxtimes\psi}{\phi\tens\psi}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:parr universal}
		\(\parr\) is universal wrt. its rules. 
		\newcommand\barr{\mathbin\wp}
		\[
			\inference[\nameref{ax:right parr}]{
				\binaryinf[\(\barr\)L]{
					\judgement\phi\phi
				}{
					\judgement\psi\psi
				}{
					\judgement{\phi\barr\psi}{\phi,\psi}
				}
			}{
				\judgement{\phi\barr\psi}{\phi\parr\psi}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:with is universal}
		\(\with\) is universal wrt. its rules.
		\newcommand\beef{\mathbin{\varepsilon}}
		\[
			\binaryinf[\nameref{ax:right with}]{
				\inference[\(\beef\)L\(_1\)]{
					\inference[\nameref{ax:identity}]{}
						{\judgement{\phi}{\phi}}
				}{
					\judgement{\phi\beef\psi}{\phi}
				}
			}{
				\inference[\(\beef\)L\(_2\)]{
					\inference[\nameref{ax:identity}]{}
						{\judgement{\psi}{\psi}}
				}{
					\judgement{\phi\beef\psi}{\psi}
				}
			}{
				\judgement{\phi\beef\psi}{\phi\with\psi}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:1 is universal}
		\(1\) is universal wrt. its rules.
		\[
			\inference[\(1'\):L]{
				\inference[\nameref{ax:right 1}]{}
					{\judgement{}{1}}
			}{
				\judgement{1'}{1}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:bot is universal}
		\(\bot\) is universal wrt. its rules.
		\[
			\inference[\nameref{ax:right bot}]{
				\inference[\(\bot'\):L]{}
					{\judgement{\bot'}{}}
			}{
				\judgement{\bot'}{\bot}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:0 is universal}
		\(0\) is universal wrt. its rule.
		\[
			\inference[\(0'\):L]{}{
				\judgement{0'}{0}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}

	\begin{theorem}
		\label{thm:top is universal}
		\(\top\) is universal wrt. its rule.
		\[
			\inference[\nameref{ax:right top}]{}{
				\judgement{\top'}{\top}
			}
		\]
		\textit{mutatis mutandis}, the converse holds.
	\end{theorem}
	
	\begin{theorem}[\(\tens\)Sym]
		\label{thm:tens symmetric}
		\[
		\inference[\nameref{ax:left tens}]
		{	\inference[\nameref{ax:left exchange}] 
			{	\binaryinf[\nameref{ax:right tens}]
				{	\inference[\nameref{ax:identity}]
					{}{	\judgement
						{\alpha}
						{\alpha}
					}
				}{	\inference[\nameref{ax:identity}]
					{}{	\judgement
						{\beta}
						{\beta}
					}
				}{	\judgement
						{\alpha,\beta}
						{\alpha\tens\beta}
				}
			}{	\judgement
					{\beta,\alpha}
					{\alpha\tens\beta}
			}
		}{	\judgement
				{\beta\tens\alpha}
				{\alpha\tens\beta}
		}
		\]
	\end{theorem}

	\begin{theorem}[\(\parr\)Sym]
		\label{thm:parr symmetric}
		\[
		\inference[\nameref{ax:right parr}] 
		{	\inference[\nameref{ax:right exchange}] 
			{	\binaryinf[\nameref{ax:left parr}] 
				{	\inference[\nameref{ax:identity}]
					{}{	\judgement
							{\alpha}
							{\alpha}
					}
				}{	\inference[\nameref{ax:identity}]
					{}{	\judgement
							{\beta}
							{\beta}
					}
				}{	\judgement
						{\alpha\parr\beta}
						{\alpha,\beta}
				}
			}{	\judgement
					{\alpha\parr\beta}
					{\beta, \alpha}
			}
		}{	\judgement
				{\alpha\parr\beta}
				{\beta\parr\alpha}
		}
		\]
	\end{theorem}
	
	\begin{theorem}[\(\plus\)Sym]
		\[
			\binaryinf[\nameref{ax:left plus}]{
				\inference[\nameref{ax:right plus 1}]{
					\inference[\nameref{ax:identity}]{}{
						\judgement\phi\phi
					}
				}{
					\judgement{\phi}{\psi\plus\phi}
				}
			}{
				\inference[\nameref{ax:right plus 0}]{
					\inference[\nameref{ax:identity}]{}{
						\judgement\psi\psi
					}
				}{
					\judgement{\psi}{\psi\plus\phi}
				}
			}{
				\judgement{\phi\plus\psi}{\psi\plus\phi}
			}
		\]
	\end{theorem}

	\begin{theorem}[\(\with\)Sym]
		\[
			\binaryinf[\nameref{ax:right with}]{
				\inference[\nameref{ax:left with 1}]{
					\inference[\nameref{ax:identity}]{}{
						\judgement\phi\phi
					}
				}{
					\judgement{\psi\with\phi}{\phi}
				}
			}{
				\inference[\nameref{ax:left with 0}]{
					\inference[\nameref{ax:identity}]{}{
						\judgement\psi\psi
					}
				}{
					\judgement{\psi\with\phi}{\psi}
				}
			}{
				\judgement{\phi\with\psi}{\psi\with\phi}
			}
		\]
	\end{theorem}

	\begin{theorem}[\(\parr\)LEM]
		\[
			\inference[\nameref{thm:parr symmetric}] 
			{	\inference[\nameref{ax:right parr}] 
				{	\inference[\nameref{ax:right dual}] 
					{	\inference[\nameref{ax:identity}]
						{}{	\judgement
								{\alpha}
								{\alpha}
						}
					}{	\judgement
							{}
							{\dual\alpha,\alpha}
					}
				}{	\judgement
						{}
						{\dual\alpha\parr\alpha}
				}
			}{	\judgement
					{}
					{\alpha\parr\dual\alpha}
			}
		\]
	\end{theorem}

	\begin{theorem}[\(\dual{}\)R\(\dual{}\)L]
		\namelabel{\(\dual\_\)Cv}
		\label{thm:dual contravariance}
		\[
		\inference[\nameref{ax:left dual}]
		{	\inference[\nameref{ax:right dual}]
			{	\judgement
					\phi
					\psi
			}{	\judgement
				{}
				{\dual\phi,\psi}
			}
		}{	\judgement
				{\dual\psi}
				{\dual\phi}
		}
		\]
	\end{theorem}

	\begin{theorem}[\(\dual{}\dual{}\)]
		\label{thm:bidual elimination}
		\[
			\inference[\nameref{ax:left dual}]
			{	\inference[\nameref{ax:right dual}]
				{	\inference[\nameref{ax:identity}]
					{}{	\judgement
						{\phi}
						{\phi}
					}
				}{	\judgement
						{}
						{\dual\phi,\phi}
				}
			}{	\judgement
					{\bidual\phi}
					{\phi}
			}
			\hspace{4em}
			\inference[\nameref{ax:right dual}]
			{	\inference[\nameref{ax:left dual}]
				{	\inference[\nameref{ax:identity}]
					{}{	\judgement
						{\phi}
						{\phi}
					}
				}{	\judgement
						{\phi, \dual\phi}
						{}
				}
			}{	\judgement
					{\phi}
					{\bidual\phi}
			}
		\]
	\end{theorem}
	\begin{theorem}[\(\dual\parr\)]
		\[
			\inference[\nameref{ax:right tens}]
			{	\inference[\nameref{ax:right dual}]
				{	\inference[\nameref{ax:right dual}]
					{	\inference[\nameref{ax:right exchange}]
						{	\inference[\nameref{ax:left dual}]
							{	\binaryinf[\nameref{ax:right parr}]
								{	\inference[\nameref{ax:identity}]
									{}{	\judgement
										{\alpha}
										{\alpha}
									}
								}{	\inference[\nameref{ax:identity}]
									{}{	\judgement
										{\beta}
										{\beta}
									}
								}{	\judgement
										{\alpha,\beta}
										{\alpha\parr\beta}
								}
							}{	\judgement
								{\alpha,\beta,\dual{(\alpha\parr\beta)}}
								{}
							}
						}{	\judgement
								{\dual{(\alpha\parr\beta)},\alpha,\beta}
								{}
						}
					}{	\judgement
							{\dual{(\alpha\parr\beta)},\alpha}
							{\dual\beta}
					}
				}{	\judgement
					{\dual{(\alpha\parr\beta)}}
					{\dual\alpha,\dual\beta}
				}
			}{	\judgement
					{\dual{(\alpha\parr\beta)}}
					{\dual\alpha\tens\dual\beta}
			}
		\]
		\[
			\inference[\nameref{ax:right dual}]
			{	\inference[\nameref{ax:left parr}]
				{	\inference[\nameref{thm:bidual elimination}\(\times\)2]
					{	\inference[\nameref{ax:left dual}\(\times\)2]
						{	\binaryinf[\nameref{ax:left tens}]
							{	\inference[\nameref{ax:identity}]
								{}{	\judgement
									{\dual\alpha}
									{\dual\alpha}
								}
							}{	\inference[\nameref{ax:identity}]
								{}{	\judgement
									{\dual\beta}
									{\dual\beta}
								}
							}{	\judgement
									{\dual\alpha\tens\dual\beta}
									{\dual\alpha,\dual\beta}
							}
						}{	\judgement
							{\dual\alpha\tens\dual\beta,\bidual\alpha,\bidual\beta}
							{}
						}
					}{	\judgement
							{\dual\alpha\tens\dual\beta,\alpha,\beta}
							{}
					}
				}{	\judgement
						{\dual\alpha\tens\dual\beta,\alpha\parr\beta}
						{}
				}
			}{	\judgement
					{\dual\alpha\tens\dual\beta}
					{\dual{(\alpha\parr\beta)}}
			}
		\]
	\end{theorem}

	\begin{theorem}[\(\dual\tens\)]
		\[
			\dual{(\alpha\tens\beta)}
			\lequiv
			\dual{(\bidual\alpha\tens\bidual\beta)}	
			\lequiv
			\bidual{(\dual\alpha\parr\dual\beta)}
			\lequiv
			\dual\alpha\parr\dual\beta
		\]
	\end{theorem}

	\begin{theorem}[\(\tens\cdot\plus\)]
		\label{thm:tens plus distrib}
		\fontsize{5pt}{5pt}\selectfont
		\[
			\binaryinf[\nameref{ax:left plus}]
			{	\inference[\nameref{ax:right plus 0}]
				{	\binaryinf[\nameref{ax:right tens}]
					{	\inference[\nameref{ax:identity}]
						{}{	\judgement
								{\alpha}
								{\alpha}
						}
					}{	\inference[\nameref{ax:identity}]
						{}{	\judgement
							{\beta_0}
							{\beta_0}}
					}{	\judgement
							{\alpha, \beta_0}
							{(\alpha\tens\beta_0)}
					}
				}{	\judgement
						{\alpha, \beta_0}
						{(\alpha\tens\beta_0)\plus(\alpha\tens\beta_1)}
				}
			}{	\inference[\nameref{ax:right plus 1}]
				{	\binaryinf[\nameref{ax:right tens}]
					{	\inference[\nameref{ax:identity}]
						{}{	\judgement
								{\alpha}
								{\alpha}
						}
					}{	\inference[\nameref{ax:identity}]
						{}{	\judgement
							{\beta_1}
							{\beta_1}}
					}{	\judgement
							{\alpha, \beta_1}
							{(\alpha\tens\beta_1)}
					}
				}{	\judgement
						{\alpha, \beta_1}
						{(\alpha\tens\beta_0)\plus(\alpha\tens\beta_1)}
				}
			}{	\inference[\nameref{ax:left tens}]
				{	\judgement
						{\alpha, (\beta_0\plus\beta_1)}
						{(\alpha\tens\beta_0)\plus(\alpha\tens\beta_1)}
				}{	\judgement
						{\alpha\tens(\beta_0\plus\beta_1)}
						{(\alpha\tens\beta_0)\plus(\alpha\tens\beta_1)}
				}
			}
		\]
		\[
			\binaryinf[\nameref{ax:left plus}]	
			{	\inference[\nameref{ax:left tens}]
				{	\binaryinf[\nameref{ax:right tens}]
					{	\inference[\nameref{ax:identity}]
						{}{	\judgement
								{\alpha}
								{\alpha}
						}
					}{	\inference[\nameref{ax:right plus 0}]
						{	\inference[\nameref{ax:identity}]
							{}{	\judgement
								{\beta_0}
								{\beta_0}
							}
						}{	\judgement
								{\beta_0}
								{\beta_0\plus\beta_1}
						}
					}{	\judgement
						{\alpha,\beta_0}
						{\alpha\tens(\beta_0\plus\beta_1)}
					}
				}{	\judgement
					{\alpha\tens\beta_1}
					{\alpha\tens(\beta_0\plus\beta_1)}
				}
			}{	\inference[\nameref{ax:left tens}]
				{	\binaryinf[\nameref{ax:right tens}]
					{	\inference[\nameref{ax:identity}]
						{}{	\judgement
								{\alpha}
								{\alpha}
						}
					}{	\inference[\nameref{ax:right plus 1}]
						{	\inference[\nameref{ax:identity}]
							{}{	\judgement
								{\beta_1}
								{\beta_1}
							}
						}{	\judgement
								{\beta_1}
								{\beta_0\plus\beta_1}
						}
					}{	\judgement
						{\alpha,\beta_1}
						{\alpha\tens(\beta_0\plus\beta_1)}
					}
				}{	\judgement
					{\alpha\tens\beta_1}
					{\alpha\tens(\beta_0\plus\beta_1)}
				}
			}{	\judgement
				{(\alpha\tens\beta_0)\plus(\alpha\tens\beta_1)}
				{\alpha\tens(\beta_0\plus\beta_1)}
			}
		\]
	\end{theorem}

	\begin{theorem}[\(\with\cdot\parr\)]
		\label{thm:with parr distrib}
		\fontsize{5pt}{5pt}\selectfont
		\[
			\binaryinf[\nameref{ax:right with}]{
				\inference[\nameref{ax:right parr}]{
					\binaryinf[\nameref{ax:left parr}] {
						\inference[\nameref{ax:identity}]{}{
							\judgement
								{\phi}
								{\phi}
						}
					}{
						\inference[\nameref{ax:left with 1}]{
							\inference[\nameref{ax:identity}]{
							}{
								\judgement
									{\xi}
									{\xi}
							}
						}{
							\judgement
								{\psi\with\xi}
								{\xi}
						}
					}{
						\judgement
							{\phi\parr(\psi\with\xi)}
							{\phi,\xi}
					}
				}{
					\judgement
						{\phi\parr(\psi\with\xi)}
						{\phi\parr\xi}
				}
			}{
				\inference[\nameref{ax:right parr}]{
					\binaryinf[\nameref{ax:left parr}] {
						\inference[\nameref{ax:identity}]{}{
							\judgement
								{\phi}
								{\phi}
						}
					}{
						\inference[\nameref{ax:left with 0}]{
							\inference[\nameref{ax:identity}]{
							}{
								\judgement
									{\psi}
									{\psi}
							}
						}{
							\judgement
								{\psi\with\xi}
								{\psi}
						}
					}{
						\judgement
							{\phi\parr(\psi\with\xi)}
							{\phi,\psi}
					}
				}{
					\judgement
						{\phi\parr(\psi\with\xi)}
						{\phi\parr\psi}
				}
			}{
				\judgement
					{\phi\parr(\psi\with\xi)}
					{(\phi\parr\psi)\with(\phi\parr\xi)}
			}
		\]

		\[
			\inference[\nameref{ax:left parr}]{
				\binaryinf[\nameref{ax:right with}]{
					\inference[\nameref{ax:left with 1}]{
						\binaryinf[\nameref{ax:left parr}]{
							\inference[\nameref{ax:identity}]{}{
								\judgement{
									\phi
								}{
									\phi
								}
							}
						}{
							\inference[\nameref{ax:identity}]{}{
								\judgement{
									\xi
								}{
									\xi
								}
							}
						}{
							\judgement{
								\phi\parr\xi
							}{
								\phi,\xi
							}
						}
					}{
						\judgement{
							(\phi\parr\psi)\with(\phi\parr\xi)
						}{
							\phi,\xi
						}
					}
				}{
					\inference[\nameref{ax:left with 0}]{
						\binaryinf[\nameref{ax:left parr}]{
							\inference[\nameref{ax:identity}]{}{
								\judgement{
									\phi
								}{
									\phi
								}
							}
						}{
							\inference[\nameref{ax:identity}]{}{
								\judgement{
									\psi
								}{
									\psi
								}
							}
						}{
							\judgement{
								\phi\parr\psi
							}{
								\phi,\psi
							}
						}
					}{
						\judgement{
							(\phi\parr\psi)\with(\phi\parr\xi)
						}{
							\phi,\psi
						}
					}
				}{
					\judgement{
						(\phi\parr\psi)\with(\phi\parr\xi)
					}{
						\phi,(\psi\with\xi)
					}
				}
			}{
				\judgement{
					(\phi\parr\psi)\with(\phi\parr\xi)
				}{
					\phi\parr(\psi\with\xi)
				}
			}
		\]
	\end{theorem}

	\begin{theorem}[Units]
		\label{thm:units}
		\[
			\binaryinf[\nameref{ax:right tens}]{
				\inference{}{\judgement\phi\phi}
			}{
				\inference[\nameref{ax:right 1}]{}{
					\judgement{}1
				}
			}{
				\judgement\phi{\phi\tens1}
			}
			\quad
			\inference[\nameref{ax:left tens}]{
				\inference[\nameref{ax:left 1}]{
					\inference{}{
						\judgement\phi\phi 
					}
				}{
					\judgement{\phi,1}\phi
				}
			}{
				\judgement{\phi\tens1}\phi
			}
		\]
		\[
			\inference[\nameref{ax:right parr}]{
				\inference[\nameref{ax:right bot}]{
					\inference{}{\judgement\phi\phi}
				}{
					\judgement\phi{\phi,\bot}
				}{
				}
			}{
				\judgement\phi{\phi\parr\bot}
			}
			\quad
			\binaryinf[\nameref{ax:left parr}]{
				\inference{}{
					\judgement\phi\phi
				}
			}{
				\inference[\nameref{ax:left bot}]{}{
					\judgement\bot{}
				}
			}{
				\judgement{\phi\parr\bot}\phi
			}
		\]
		\[
			\binaryinf[\nameref{ax:right with}]{
				\inference{}{\judgement\phi\phi}
			}{
				\inference[\nameref{ax:right top}]{}{
					\judgement\phi\top
				}
			}{
				\judgement\phi{\phi\with\top}
			}
			\quad
			\inference[\nameref{ax:left with 0}]{
				\inference{}{\judgement\phi\phi}
			}{
				\judgement{\phi\with\top}\phi
			}
		\]
		\[
			\inference[\nameref{ax:right plus 0}]{
				\inference{}{\judgement\phi\phi}
			}{
				\judgement\phi{\phi\plus 0}
			}
			\quad
			\binaryinf[\nameref{ax:left plus}]{
				\inference[\nameref{ax:left 0}]{}{
					\judgement 0\phi
				}
			}{
				\inference{}{
					\judgement\phi\phi
				}
			}{
				\judgement{\phi\plus 0}\phi
			}
		\]
	\end{theorem}

	\begin{theorem}[Nullits]
		\label{thm:nullities}
		\[
			\inference[\nameref{ax:left 0}]{}{
				\judgement 0 {\phi\tens 0}
			}
			\quad
			\inference[\nameref{ax:left tens}]{
				\inference[\nameref{ax:left 0}]{}{
					\judgement {\phi, 0} 0
				}
			}{
				\judgement {\phi\tens 0} 0
			}
		\]
		\[
			\inference[\nameref{ax:right parr}]{
				\inference[\nameref{ax:right top}]{}{
					\judgement\top{\phi,\top}
				}
			}{
				\judgement\top{\phi\parr\top}
			}
			\quad
			\inference[\nameref{ax:right top}]{}{
				\judgement{\phi\parr\top}\top
			}
		\]
	\end{theorem}

	\begin{theorem}[!-?]
		\label{thm:exponential duality}
		\[
			\inference[\nameref{ax:left dual}]{
				\inference[\nameref{ax:right !}]{
					\inference[\nameref{ax:right dual}]{
						\inference[\nameref{ax:right ?}]{
							\inference{}{
								\judgement\phi\phi
							}
						}{
							\judgement\phi{
								?\phi
							}
						}
					}{
						\judgement{}{
							\dual\phi,
							?\phi
						}
					}
				}{
					\judgement{}{
						!\dual\phi,
						?\phi
					}
				}
			}{
				\judgement{
					\dual{(!\dual\phi)}
				}{
					?\phi
				}
			}
		\]
		\[
			\inference[\nameref{ax:right dual}]{
				\inference[\nameref{ax:left ?}]{
					\inference[\cref{thm:bidual elimination}]{
						\inference[\nameref{ax:left dual}]{
							\inference[\nameref{ax:left !}]{
								\inference{}{
									\judgement{\dual\phi}{\dual\phi}
								}
							}{
								\judgement{!\dual\phi}{\dual\phi}
							}
						}{
							\judgement{\bidual\phi,!\dual\phi}{}
						}
					}{
						\judgement{\phi,!\dual\phi}{}
					}
				}{
					\judgement{?\phi,!\dual\phi}{}
				}
			}{
				\judgement{?\phi}{\dual{(!\dual\phi)}}
			}
		\]
		and consequently we too have that 
		\[
			\dual{(?\dual\phi)}\lequiv !\phi
		\]
	\end{theorem}
\end{document}
